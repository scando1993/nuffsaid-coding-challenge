def custom_split(string):
    result_list = []

    if not string:
        return [string]
    start = 0
    inner_cont = False
    for index, char in enumerate(string):
        if char == ',':
            if not inner_cont:
                result_list.append(string[start:index])
                start = index + 1
        elif char == '"':
            if not inner_cont:
                start = index + 1
            inner_cont = not inner_cont

    if start == 0:
        return [string]
    result_list.append(string[start:index + 1])

    return result_list


class NCESData:
    def __init__(self, filename, header=False):
        self.dataset = []
        self.header = []
        self.filename = filename
        self.load_data(header)

    def load_data(self, header=False):
        with open(self.filename, 'r', encoding='cp1252') as f:
            lines = f.readlines()
            if header:
                self.header.append(lines[0].strip().split(','))
            for line in lines[1:]:
                line = line.strip()
                self.dataset.append(custom_split(line))

    def get_schools(self):
        schools = []
        for data in self.dataset:
            school = data[4]
            schools.append(school)

        return schools

    def get_cities(self):
        cities = set()
        for data in self.dataset:
            city = data[4]
            cities.add(city)

        return cities

    def get_states(self):
        states = []
        for data in self.dataset:
            state = data[4]
            states.append(state)

        return states

    def total_schools(self):
        schools = dict()
        for data in self.dataset:
            state = data[0]
            if state in schools.keys():
                schools[state] += 1
            else:
                schools[state] = 1

        return len(schools.keys())

    def schools_per_state(self):
        schools = dict()
        for data in self.dataset:
            state = data[5]
            if state in schools.keys():
                schools[state] += 1
            else:
                schools[state] = 1

        return schools

    def schools_in_metro(self):
        schools = dict()
        for data in self.dataset:
            state = data[-3]
            if state in schools.keys():
                schools[state] += 1
            else:
                schools[state] = 1

        return schools

    def city_with_most_schools(self):
        schools = dict()
        max_schools = 0
        key = ''
        for data in self.dataset:
            city = data[4]
            if city in schools.keys():
                schools[city] += 1
                if schools[city] >= max_schools:
                    max_schools = schools[city]
                    key = city
            else:
                schools[city] = 1

        return key, max_schools

    def unique_cities_with_one_school(self):
        schools = dict()
        cities = []
        for data in self.dataset:
            city = data[4]
            if city in schools.keys():
                schools[city] += 1
                if city in cities:
                    cities.remove(city)
            else:
                schools[city] = 1
                cities.append(city)

        return len(cities)
