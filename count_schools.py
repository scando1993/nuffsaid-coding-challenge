from ncesdata import NCESData


def print_dict(_dict):
    for key in _dict.keys():
        print(str(key) + ": " + str(_dict[key]))


def print_counts():
    data = NCESData('school_data.csv')
    print('Total Schools: ' + str(data.total_schools()))
    print('Schools by State: ')
    print_dict(data.schools_per_state())
    print('Schools by Metro-centric locale: ')
    print_dict(data.schools_in_metro())
    city, schools = data.city_with_most_schools()
    print('City with most schools: ' + city + " (%s schools)" % schools)
    print('Unique cities with at least one school: ' + str(data.unique_cities_with_one_school()))


if __name__ == '__main__':
    print_counts()
