class PriorityQueue:
    def __init__(self):
        self.heap = []

    def is_empty(self):
        return True if len(self.heap) == 0 else False

    def push(self, val):
        cur = len(self.heap)
        self.heap.append(val)
        while cur > 0:
            parent = (cur - 1) // 2
            # if self.heap[parent] <= self.heap[cur]:
            if self.heap[parent] > self.heap[cur]:
                break
            self.heap[cur], self.heap[parent] = self.heap[parent], self.heap[cur]
            cur = parent

    def pop(self):
        ret = self.heap[0]
        last = self.heap.pop()
        size = len(self.heap)
        if size == 0:
            return ret
        self.heap[0] = last
        cur = 0
        while True:
            ch1 = 2 * cur + 1
            if ch1 >= size:
                return ret
            ch2 = ch1 + 1
            # child = ch2 if ch2 < size and self.heap[ch2] < self.heap[ch1] else ch1
            child = ch2 if ch2 < size and self.heap[ch2] >= self.heap[ch1] else ch1
            # if self.heap[cur] <= self.heap[child]:
            if self.heap[cur] > self.heap[child]:
                return ret
            self.heap[child], self.heap[cur] = self.heap[cur], self.heap[child]
            cur = child


if __name__ == "__main__":
    pq = PriorityQueue()
    pq.push((4, object))
    pq.push((10, object))
    pq.push((1, object))
    pq.push((100, object))
    pq.push((0, object))
    pq.push((20, object))
    # pq.push(4)
    # pq.push(10)
    # pq.push(1)
    # pq.push(100)
    # pq.push(0)
    # pq.push(20)

    while not pq.is_empty():
        print(pq.pop())
