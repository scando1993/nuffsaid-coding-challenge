import time

from ncesfilter import NCESFilter


def search_schools(string):
    filter = NCESFilter('school_data.csv')
    start_time = time.perf_counter()
    search = filter.search(string)
    end_time = time.perf_counter()
    execution_time = end_time - start_time
    print(f'Results for "{string}" (search took: {execution_time * 1000}ms)')
    pq, pq_possible = search
    i = 0
    while not pq.is_empty():
        _, results = pq.pop()
        print(f'{i + 1}. {results[0]}\n{results[1]}, {results[2]}')
        i += 1
    if not pq_possible.is_empty():
        i = 0
        print(f'Suggestions for "{string}"')
        while not pq_possible.is_empty():
            _, results = pq_possible.pop()
            print(f'{i + 1}. {results[0]}\n{results[1]}, {results[2]}')
            i += 1


if __name__ == '__main__':
    search_schools("ELEMENTARY SCHOOL HIGHLAND PARK")
    search_schools("jefferson belleville")
    search_schools("riverside school 44")
    search_schools("granada charter school")
    search_schools("foley high alabama")