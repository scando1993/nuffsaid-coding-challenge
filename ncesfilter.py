from ncesdata import NCESData
from trie import Trie
from ahocorasick import AhoCorasick
import itertools
from priorityqueue import PriorityQueue

STATES = {
    'ALASKA': 'AK',
    'ALABAMA': 'AL',
    'ARKANSAS': 'AR',
    'ARIZONA': 'AZ',
    'CALIFORNIA': 'CA',
    'COLORADO': 'CO',
    'CONNECTICUT': 'CT',
    'DISTRICT OF COLUMBIA': 'DC',
    'DELAWARE': 'DE',
    'FLORIDA': 'FL',
    'GEORGIA': 'GA',
    'HAWAII': 'HI',
    'IOWA': 'IA',
    'IDAHO': 'ID',
    'ILLINOIS': 'IL',
    'INDIANA': 'IN',
    'KANSAS': 'KS',
    'KENTUCKY': 'KY',
    'LOUISIANA': 'LA',
    'MASSACHUSETTS': 'MA',
    'MARYLAND': 'MD',
    'MAINE': 'ME',
    'MICHIGAN': 'MI',
    'MINNESOTA': 'MN',
    'MISSOURI': 'MO',
    'MISSISSIPPI': 'MS',
    'MONTANA': 'MT',
    'NORTH CAROLINA': 'NC',
    'NORTH DAKOTA': 'ND',
    'NEBRASKA': 'NE',
    'NEW HAMPSHIRE': 'NH',
    'NEW JERSEY': 'NJ',
    'NEW MEXICO': 'NM',
    'NEVADA': 'NV',
    'NEW YORK': 'NY',
    'OHIO': 'OH',
    'OKLAHOMA': 'OK',
    'OREGON': 'OR',
    'PENNSYLVANIA': 'PA',
    'RHODE ISLAND': 'RI',
    'SOUTH CAROLINA': 'SC',
    'SOUTH DAKOTA': 'SD',
    'TENNESSEE': 'TN',
    'TEXAS': 'TX',
    'UTAH': 'UT',
    'VIRGINIA': 'VA',
    'VERMONT': 'VT',
    'WASHINGTON': 'WA',
    'WISCONSIN': 'WI',
    'WEST VIRGINIA': 'WV',
    'WYOMING': 'WY',
}


class NCESFilter:
    def __init__(self, filename):
        ncesdata = NCESData(filename)
        self.school_trie = Trie()
        self.cities_trie = Trie()
        self.states_trie = Trie()
        self.schools = dict()
        self.cities = dict()
        self.states = dict()
        self.index(ncesdata.dataset)
        for school in self.schools.keys():
            self.school_trie.insert(school)
        for city in self.cities.keys():
            self.cities_trie.insert(city)
        for state in STATES.keys():
            self.states_trie.insert(state)

    def index(self, dataset):
        for data in dataset[:]:
            school, city, state = (data[3], data[4], data[5])
            if school in self.schools.keys():
                self.schools[school][city] = state
            else:
                self.schools[school] = {city: state}
            if city in self.cities.keys():
                self.cities[city].append(school)
            else:
                self.cities[city] = [school]
            if state in self.states.keys():
                if city in self.states[state].keys():
                    self.states[state][city].append(school)
                else:
                    self.states[state][city] = [school]
            else:
                self.states[state] = {city: [school]}

    def search_school(self, query_search):
        x = set()
        search = query_search
        for element in list(itertools.permutations(search, len(search))):
            trie_search = self.school_trie.query(" ".join(element))
            x.update(trie_search)
        return x

    def search_cities(self, query_search):
        cities = set()
        possible_cities = set()
        exact_match = False
        for element in query_search:
            trie_search = self.cities_trie.query(element)
            possible_cities.update(trie_search)
            if element in trie_search:
                exact_match = True
                possible_cities.remove(element)
                cities.add(element)
        return exact_match, cities, possible_cities

    def search_states(self, query_search):
        states = set()
        possible_states = set()
        exact_match = False
        for element in query_search:
            trie_search = self.states_trie.query(element)
            for _element in trie_search:
                possible_states.add(_element)
            if element in trie_search:
                exact_match = True
                possible_states.remove(element)
                states.add(element)
        return exact_match, states, possible_states

    def search(self, query):
        search = query.upper().split(" ")
        aho_corasick = AhoCorasick(search.copy())
        pq = PriorityQueue()
        pq_possible = PriorityQueue()

        schools = self.search_school(search)

        if schools:
            for school in schools:
                words = aho_corasick.search_words(school)
                for city in self.schools[school].keys():
                    if len(words.keys()):
                        pq.push((len(words.keys()), [school, city, self.schools[school][city]]))
            return pq, pq_possible

        city_found, _cities, _possible_cities = self.search_cities(search)
        state_found, _states, _possible_states = self.search_states(search)

        if state_found and city_found:
            for state in _states:
                for city in _cities:
                    for school in self.states[STATES[state]][city]:
                        words = aho_corasick.search_words(school)
                        if len(words) > 0:
                            pq.push((len(words.keys()), [school, city, state]))
        elif state_found:
            for state in _states:
                for city in self.states[STATES[state]]:
                    for school in self.states[STATES[state]][city]:
                        words = aho_corasick.search_words(school)
                        if len(words.keys()) > 0:
                            pq.push((len(words.keys()), [school, city, STATES[state]]))
            for state in _possible_states:
                for city in self.states[STATES[state]]:
                    for school in self.states[STATES[state]][city]:
                        words = aho_corasick.search_words(school)
                        if len(words.keys()):
                            pq_possible.push((len(words.keys()), [school, city, STATES[state]]))
        elif city_found:
            for city in _cities:
                for school in self.cities[city]:
                    words = aho_corasick.search_words(school)
                    if len(words.keys()) > 0 and city not in words.keys():
                        pq.push((len(words.keys()), [school, city, self.schools[school][city]]))
            for city in _possible_cities:
                for school in self.cities[city]:
                    words = aho_corasick.search_words(school)
                    if len(words.keys()) > 0 and city not in words.keys():
                        pq_possible.push((len(words.keys()), [school, city, self.schools[school][city]]))

        return pq, pq_possible


if __name__ == '__main__':
    filter = NCESFilter('sl051bai.csv')
    print(filter.search("ELEMENTARY SCHOOL HIGHLAND PARK"))
    print(filter.search("jefferson belleville"))
    print(filter.search("riverside school 44"))
    print(filter.search("granada charter school"))
    print(filter.search("foley high alabama"))